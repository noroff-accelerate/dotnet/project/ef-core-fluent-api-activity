﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.FluentAPIActivity.Data.Entities
{
    public class Instructor
    {
        public int InstructorId { get; set; }
        public string Name { get; set; }
        public int DepartmentId { get; set; } // Foreign key for Department
        public Department Department { get; set; }
        public ICollection<Course> Courses { get; set; }
    }
}

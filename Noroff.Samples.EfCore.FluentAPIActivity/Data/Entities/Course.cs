﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.FluentAPIActivity.Data.Entities
{
    public class Course
    {
        public int CourseId { get; set; }
        public string Title { get; set; }
        public int Credits { get; set; }
        public bool IsOffered { get; set; } // Flag to indicate if the course is currently offered
        public int InstructorId { get; set; } // Foreign key for Instructor
        public Instructor Instructor { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
